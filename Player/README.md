# Player
#### Public
| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| getLoginType |  | Int | get login type<br>6001: EVENT_LOGIN_JOIN<br>6002:EVENT_LOGIN_BIND<br>6003:EVENT_LOGIN_LOGIN |
| getUID |  | String | get uid (the identification of user) |
| getDeviceID |  | String | get device id |
| getGoogleID | | String | get user's Google ID if login with Google api |
| getFacebookID |  | String | get user's Facebook ID if login with Facebook api |
| getServerID |  | String | get user's server id |
| getBindUserfrom |  | int | get user's bind from |
| displayInfo() | | void | output user's information and display with log|
