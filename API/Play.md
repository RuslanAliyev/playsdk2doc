# Play
`Play have four methods to enter game`
1. Fast Play : Enter with Fast Play login
2. Facebook Play : Enter with Facebook login
3. Google Play : Enter with google login
4. Play9388 Play : Enter with Play9388 login
5. Player Play : System will enter with the recent type of login using the corresponding Play method.

> The method is used to set server's id which player select and verify account if normal.

### Fast Play
```java
playSDK.api().doFastPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Facebook Play
```java
playSDK.api().doFacebookPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Google Play
```java
playSDK.api().doGooglePlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Play9388 Play
```java
playSDK.api().doPlay9388Play(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Player Play
```java
playSDK.api().doPlayerPlay(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```
