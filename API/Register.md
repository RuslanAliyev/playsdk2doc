# Register
`Play SDK register have four methods`
1. Fast Register : Register with identification of device
2. Facebook Register : Register with Facebook api
3. Google Register : Register with google api
4. Play9388 Register : Register with 9388's username

> The method is used to register if don't exist any login information and choose other account.

### Fast Register
```java
playSDK.api().doFastRegister(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Facebook Register
```java
playSDK.api().doFacebookRegister(Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Google Register
```java
playSDK.api().doGoogleRegister(ConnectionResult, GoogleApiClient, Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Play9388 Register
```java
playSDK.api().doPlay9388Register(Response, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```
