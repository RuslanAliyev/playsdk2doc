# Other action
### Get settings of 9388 ball
Before display 9388 ball,you have to call this api to get the account's setting and privilage about 9388 ball.
```java
playSDK.api().doGameControl(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### get list of purchace product and id at server
```java
playSDK.api().doProductID(Activity, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### excute the procedure of google's in app purchase
```java
playSDK.api().doInAppBilling(Activity, appsorderid, receipt, signature, new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### send GCM Register ID to sdk server
```java
playSDK.api().doPush(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### logout
```java
playSDK.api().doLogout(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### login page
launch the login activity
```java
playSDK.api().startLoginActivity(Activity);
```
feedback of login activity
```java
// Play SDK feedback of login activity
playSDK.api().onLoginActivityResult(this, requestCode, resultCode, data,
new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // if connection error then..
    // [for example:start login activity again]
    playSDK.api().startLoginActivity(LoginActivity.this);
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    if (responseClient.getEvent_code() == EventCode.EVENT_LOGIN_ERROR) {
      // if player leave login activity with the unexpected behavior like touch button of back or home , pick up someone's calling, or any else
      // [example:start login activity again]
      playSDK.api().startLoginActivity(LoginActivity.this);
    } else {
      // if player finish login activity with normal process
      // [like go to the page of select game servers]
      // [the example below is the situation that user select game server and enter game world after login]

      // [example：when player select game server]
      // select game server with id=1
      playSDK.config().setServerID("1");

      // [example：when player enter game]
      // call to enter game
      playSDK.api().doPlayerPlay(new ResponseObserver() {

        @Override
        public void onError(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          // if error then...
        }

        @Override
        public void onSuccess(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          if (responseClient.isSuccess()) {
            // if success then...
            // [example：enter game world]
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          } else {
            // if error then...
          }
        }
        });
      }
    }
  });
```

### relation of 9388 ball
> you need to launch the service of 9388 ball before you display it.

launch the service of 9388 ball
```java
playSDK.api().startSDKViewService(Activity);
```
display 9388 ball
```java
playSDK.api().openSDKView(Activity);
```
close 9388 ball
```java
playSDK.api().closeSDKView(Activity);
```
stop the servie of 9388 ball
```java
playSDK.api().stopSDKViewService(Activity);
```

### convert older data
order version data need to convert to news one
```java
playSDK.api().doConvertOlderData();
```

### check if user have login the google account on phone
```java
playSDK.api().checkGoogleAccount();
```

### Facebook Share Link
```java
playSDK.api().shareLink(String link, Activity activity, ShareResponseObserver shareResponseObserver);
```
#### Implementation:
```java
playSDK.api().shareLink("http://xxx.com", MainActivity.this, new ShareResponseObserver(){
	@Override
	public void onError(String msg, FacebookException error) {

	}
	@Override
	public void onCancel(String msg) {

	}
	@Override
	public void onSuccess(String msg, Sharer.Result result) {

	}
});
```

### Facebook Share Image
```java
// For Example
// Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);

playSDK.api().shareImage(Bitmap image, Activity activity, ShareResponseObserver shareResponseObserver);
```
#### Implementation:
```java
playSDK.api().shareImage(image, MainActivity.this, new ShareResponseObserver(){
	@Override
	public void onError(String msg, FacebookException error) {

	}
	@Override
	public void onCancel(String msg) {

	}
	@Override
	public void onSuccess(String msg, Sharer.Result result) {

	}
});
```