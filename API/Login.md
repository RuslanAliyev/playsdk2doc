# Login
`Play SDK Login have four methods`
1. Fast Login : Login with identification of device
2. Facebook Login : Login with Facebook api
3. Google Login : Login with google api
4. Play9388 Login : Login with 9388's username

> The method is used to login at the begining of app when launching.Do Not use it on user's verification before entering game.

### Fast Login
```java
playSDK.api().doFastLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Facebook Login
```java
playSDK.api().doFacebookLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Google Login
```java
playSDK.api().doGoogleLogin(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```

### Play9388 Login
```java
playSDK.api().doPlay9388Login(new ResponseObserver() {
  @Override
  public void onError(ResponseClient responseClient, int event) {
    if error then...
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    if success then...
  }
});
```
