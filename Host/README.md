# Host
#### Public
| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| hasDisplayLog |  | Boolean | if setting to display log message<br> ( true: display / false: hidden ) |
| isTestEnvironment |  | Boolean | if setting test environment<br> ( true: test server / false: normal server ) |
| getGame |  | String | get game name |
| getGameVersion | | String | get game version |
| getServerID |  | String | get game server id |
| getConnectDomain |  | String | get login api url |
| getPaymentDomain |  | String | get pay api url |
| isOpendSDK | | Boolean | if display the 9388 ball |
| getServiceUrl | | String | get service url in 9388 ball |
| getGiftUrl | | String | get gift url in 9388 ball |
| getPayUrl | | String | get third party pay url in 9388 ball |
| isOpendAngel | | Boolean | if display the vip service's photo |
| getAngelPic | | String | get vip service's photo url in 9388 ball |
| getAngelUrl | | String | get vip service's introduction url in 9388 ball |
| isSDKFadeOut | | Boolean | if setting fade out effects after display 9388 ball |
| isSDKLandscape | | Boolean | if horizontal landscape of 9388 ball |
| isLoginLandscape | | Boolean | if horizontal landscape of login page |
| getCircleX | | Int | get X-coordinate of 9388 ball |
| getCircleY | | Int | get Y-coordinate of 9388 ball |
| getPlaySDKCircleText | | String | get display text of 9388 ball |
| getGCMRegID | | String | get GCM Register ID |
| getDisplayGameName | | String | get title of notice windows header |
| getDisplayGameIcon | | Int | get icon of notice windows |
| getGCMIntent() | | String | get launch activity when GCM push and click the message at screen |
| displayInfo() | | void | output host's information and display with log |
