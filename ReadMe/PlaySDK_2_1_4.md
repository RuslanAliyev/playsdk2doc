# Introduction
| item | description |
| -- | -- |
| Project name | Play SDK |
| Project version | 2.1.4 |
| Release date | 2016-08-11 |
| Developer | Ruslan Aliyev |

# Function lists
- Login with Play SDK
- float 9388 ball of Play SDK
  - Gift
  - Service
  - Payment
  - Chatroom
- Play SDK Trace Event
- Google In-app Billing for Play SDK
- Google Cloud Messaging for Play SDK

# Changes Log

1. Modified: `playSDK.api().shareLink`. Refer to 'Other' page under 'api' section for more details.

# Use Android Studio Library
put playsdk-2.1.4.aar into Libs folder of Android Studio Project

# Preliminary Setup

#### 1. AppsFlyer

1. Give your project an Application ID, like this: `applicationId "xxx.yyyyy.zz"`. This can be your project's package number. (In your application's `build.gradle` file, described below under the heading: **'what build.gradle need to set'**)
2. Contact us, tell us what your Application ID is.
3. We will give you an AppsFlyer key.
4. You put this AppsFlyer key into project, like this: `playSDK.config().setAFKey("AppsFlyer Key")`. (In your first `.java` activity's `onCreate` section, described below under the heading: **'Initial Play SDK'**)

# Setup in your codes

### what build.gradle need to set
```gradle
android {
    compileSdkVersion ...
    buildToolsVersion ...
    defaultConfig {
        applicationId "xxx.yyyyy.zz"
		...
	}
	...
}
...
repositories {
    flatDir {
        dirs 'libs'
    }
}
dependencies {
    compile 'com.forgame.playsdk:playsdk-release:2.1.4@aar'
    compile 'com.android.support:appcompat-v7:23.0.0'
    compile 'com.google.android.gms:play-services:8.3.0'
    compile 'com.facebook.android:facebook-android-sdk:4.1.0'
    compile 'com.mcxiaoke.volley:library:1.0.+@aar'
}
```
Please sync project by this way 
![Sync Project](../Images/Sync_Project_With_Gradle.png)

# Setup `AndroidManifest.xml`
```xml
<application
	android:name="com.forgame.playsdk.PlayApplication"
	android:label="@string/app_name"
	... >
	
	<meta-data
		android:name="com.facebook.sdk.ApplicationId"
		android:value="@string/facebook_app_id" />
	<meta-data
		android:name="GoogleAnalyticsId"
		android:value="[GOOGLE_ANALYTICS_ID]" />
	<meta-data
		android:name="com.google.android.gms.version"
		android:value="@integer/google_play_services_version" />
	<meta-data
		android:name="GameTag"
		android:value="[APP_NON_DISPLAY_NAME]" />
	<meta-data
		android:name="GameServerConnectKey"
		android:value="[GAME_SERVER_CONNECT_KEY]" />
	<meta-data
		android:name="ThirdPartyPayPublicKey"
		android:value="[THIRD_PARTY_PAY_PUBLIC_KEY]" />		
	<meta-data
		android:name="IapKey"
		android:value="[IN_APP_PURCHASE_PUBLIC_KEY]" />
	<meta-data
		android:name="AfKey"
		android:value="[APPSFLYER_API_KEY]" />
	... 
	
	<provider android:authorities="com.facebook.app.FacebookContentProvider[FACEBOOK_APP_ID]"
		android:name="com.facebook.FacebookContentProvider"
		android:exported="true"/>
	...
```

## Setup values/strings.xml
```xml
<resources>
    <string name="app_name">[APP_DISPLAY_NAME]</string>
    <string name="facebook_app_id">[FACEBOOK_APP_ID]</string>
</resources>
```

# Initial Play SDK 
Before using play sdk, you have to set some config which api used when connecting to server.
It usually setup it on event of onCreate at launch page of app.
You can use play sdk after the setting of below.
```java
// Declare Play SDK
PlaySDK playSDK;

// build object of Play SDK
if (playSDK == null) {
  playSDK = PlaySDK.getInstance();
}

// 'setOnWebviewCloseMethod' is optional
playSDK.config().setOnWebviewCloseMethod(new WebviewListener(){
	@Override
	public void onClose() {}
});

```

# Play SDK : package setting
Sometimes we need package lot of apk mode for different platforms.
So there are some mode which defined and can change the apk mode easier for developer.

> set the apk mode when package, and integrate relational items also.

> including enviroment of apk, display log or not,catch the version number automatically, if CB version or not, type of distribution platform, if open third party payment or not..and so on.

// the below is parameter list of modes:

| parameter | description |
| -- | -- |
| Config.BETA_CCB_ONLINE_TW | CCB,online at google play,test enviroment,google account with Taiwan |
| Config.RELEASE_CCB_ONLINE_TW | CCB,online at google play,normal enviroment,google account with Taiwan |
| Config.BETA_CCB_ONLINE_HK | CCB,online at google play,test enviroment,google account with HK |
| Config.RELEASE_CCB_ONLINE_HK | CCB,online at google play,normal enviroment,google account with HK |
| Config.BETA_CCB_OFFLINE | CCB,offline at platform,test enviroment |
| Config.RELEASE_CCB_OFFLINE | CCB,offline at platform,normal enviroment |
| Config.BETA_CB_ONLINE_TW | CB,online at google play,test enviroment,google account with Taiwan |
| Config.RELEASE_CB_ONLINE_TW | CB,online at google play,normal enviroment,google account with Taiwan |
| Config.BETA_CB_ONLINE_HK | CB,online at google play,test enviroment,google account with HK |
| Config.RELEASE_CB_ONLINE_HK | CB,online at google play,normal enviroment,google account with HK |
| Config.BETA_CB_OFFLINE | CB,offline at platform,test enviroment |
| Config.RELEASE_CB_OFFLINE | CB,offline at platform,normal enviroment |
| Config.BETA_OB_ONLINE_TW | OB,online at google play,test enviroment,google account with Taiwan |
| Config.RELEASE_OB_ONLINE_TW | OB,online at google play,normal enviroment,google account with Taiwan |
| Config.BETA_OB_ONLINE_HK | OB,online at google play,test enviroment,google account with HK |
| Config.RELEASE_OB_ONLINE_HK | OB,online at google play,normal enviroment,google account with HK |
| Config.BETA_OB_OFFLINE | OB,offline at platform,test enviroment |
| Config.RELEASE_OB_OFFLINE | OB,offline at platform,normal enviroment |

```java
playSDK.config().setAPKMode(Config.BETA_CCB_ONLINE_HK);
```

# Play SDK : life circle
the method of life circle on Activity when use Play SDK
```java
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  // ...
  // initial Play SDK completely
  // ...
  playSDK.onBuild(this);
}
public void onResume() {
  super.onResume();
  playSDK.onResume(this);
}
public void onPause() {
  super.onPause();
  playSDK.onPause(this);
}
public void onDestroy() {
  super.onDestroy();
  playSDK.onDestroy(this);
}
```

# The process flow of Play SDK
this is the process flow of Play SDK :
> main page in game => if login or not ==> not login yet => login page of Play SDK => if login success,go to the page of select server  
> login already => the page of select server => vertify user and serverid with Play SDK => enter game => launch 9388 ball with Play SDK

1. start login activity：playSDK.api().startLoginActivity(this); 
2. if login success,go to the page of select server：
  - vertify user and serverid with Play SDK after user choose server => playSDK.api().doPlayerPlay("serverid", new ResponseObserver() {....});
3. call playSDK.api().doPlayerPlay and enter game if success
4. start 9388 ball of Play SDK by call playSDK.api().openSDKView(this) after entering game;

![the process flow of Play SDK](../Images/PlaySDK_LoginFlow.png)

# Login activity
Play SDK offer Activity to used by developer，and you can deal with the feedback by onActivityResult also
### start login activity
```java
// Play SDK :start login activity
playSDK.api().startLoginActivity(this);
```
### Deal with feedback
call onActivityResult to deal with feedback of login Activity 
```java
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  super.onActivityResult(requestCode, resultCode, data);
  playSDK.onActivityResult(this, requestCode, resultCode, data, new ResponseActivityResult() {
    @Override
    public void onConnectError() {
      // if connection error then...
    }

    @Override
    public void onError() {
      // if error then...
    }

    @Override
    public void onSuccess() {
      // if success then...
      // use runOnUiThread to sure the Thread in on the Android Native Thread 
      runOnUiThread(new Runnable() {
        public void run() {
          // set serverid and vertify user before entering game
          playSDK.api()
            .doPlayerPlay("serverid", new ResponseObserver() {
            @Override
             public void onError(ResponseClient responseClient, int event) {
               // TODO Auto-generated method stub
               // if connection error then...
             }
             @Override
             public void onSuccess(ResponseClient responseClient, int event) {
               // TODO Auto-generated method stub
               // if success then...
               if (responseClient.isSuccess()) {
                 // if success before entering game then...
               } else {
                 // if error before entering game then...
               }
             }
           });
         }
       });
    }
  });
}
```

# Play SDK : 9388 ball
9388 ball will be started in the service, you can open 9388 ball at any activity of app
```java
playSDK.api().openSDKView(this);
```
close 9388 ball
```java
playSDK.api().closeSDKView(this);
```
# display third party payment always
if we need to display third party payment always like user download apk from website,you need to do the below
```java
// Display third party payment of activity
Intent intent = new Intent(this, com.forgame.playsdk.activity.PlaySDKActivity.class);
Bundle bundle = new Bundle();
bundle.putString("url", playSDK.host().getPayUrl());
intent.putExtras(bundle);
intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(intent);
```

# Payment
**the process flow of payment**
<font color="#d9534f">if Activity (Singleton), you have to add android:launchMode="singleTask" in AndroidManifest to be sure onActivityResult will go on curectly！</font>
![the process flow of payment](../Images/ForgameSDK_In-App_flow.png)

continue the payment process flow of google IAP after select the purchase
```java
playSDK.purchaseControl().purchaseFlow(this, "product ID", new PurchaseResponseObserver() {
  @Override
  public void onError(String msg, int event) {
    // TODO Auto-generated method stub
    // if error then...
  }

  @Override
  public void onSuccess(String msg, int event) {
    // TODO Auto-generated method stub
    // if success then...
  }

});
```

re-check if any purchase lost or uncomplete by call this api when going into the game's product store.
```java
playSDK.purchaseControl().checkPurchase(this);
```

# Event Trace
we have some method currently
```java
// trace the level when player reached it.the first parameter is activity, and the second is the value of level(string).
playSDK.trace().achievedLevel(this,"5");
// trace the status if completing tutorial or not.first parameter is activity, the second is the name of step, and the third is complete status(true or false)
playSDK.trace().completedTutorial(this,"First",true);
// trace when player succeed in creating role.the parameter is activity.
playSDK.trace().createRole(this);
```

# API Response
### Response event: ResponseObserver
Response of api will deal with error and success which use interface of ResponseObserver
you can describe the condition of every ResponseClient which returned by ResponseObserver
```java
playSDK.api().doPlayerPlay(new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // if error then...(including connect error)
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // if success then...
  }
});
```

### Response object: ResponseClient
#### Public
| Method | Return | Description	|
| --- | --- | --- |
| isSuccess | Booleam | if success or not |
| getCode_number | Int | get code of result |
| getEvent_code | Int | get code of event |
| getEvent_name | String | get name of event |
| getError_message | String | get error message<br>(if success will be space) |
| getData | String | get data when success |
| getResponse_timestamp | String | get timestamp of response |
| getProducts_id | ArrayList<String> | list of product id <br/>(only when feedback from getProductID API corectly)|
| displayInfo(isSuccess, title) | void | output the data of ResponseClient with log <br>isSuccess: true/false <br>title: the prefix word |
