# 版本說明
| 項目 | 描述 |
| -- | -- |
| 專案名稱 | Play SDK |
| 專案版本 | 1.5.0 |
| 發佈時間 | 2014-12-16 |
| 開發人員 | Neo Hsu |

# 目前提供功能
- [Fast] Register/Login/Play
- [Fast] Bind Facebook/Bind Google/Bind Play9388
- [Facebook] Register/Login/Play
- [Google] Register/Login/Play
- [Play9388] Register/Login/Play
- [Game] Logout
- [Game] SDK Control Setting
- [Game] Products List
<!-- - [Game] In-app Billing (Purchase/Check Purchase) -->
- [Activity] Login Activity
<!-- - [View] Float Circle View -->

# 需要啟動的權限＆服務
關於 AndroidManifest.xml 的設定與調整

### 需啟動的權限
```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="com.android.vending.BILLING" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

### 需設定 Play SDK 用到的 Activity
```xml
<activity
  android:name="com.forgame.playsdk.activity.LoginActivity"
  android:label="@string/app_name" />

<!-- Facebook login 使用的頁面 -->
<activity
  android:name="com.facebook.LoginActivity"
  android:label="@string/app_name"
  android:theme="@android:style/Theme.Translucent.NoTitleBar" />
```

### 需設定 Play SDK 初始化
Play SDK 初始化提供兩種方式  
1. 提供覆寫 Application 的方式，如遊戲內對於 Application 沒有特別用途可以直接引用 SDK 內建的方式使用
```xml
<!--
  在 AndroidManifest.xml 的 Application 中增加 android:name 參數，並使用 com.forgame.playsdk.PlayApplication
-->
<application
  android:name="com.forgame.playsdk.PlayApplication"
  android:allowBackup="true"
  android:icon="@drawable/ic_launcher"
  android:label="@string/app_name"
  android:theme="@style/AppTheme" >
```
2. 提供在 launch activity 的 onCreate/onResume 中執行下列指令 `注意此步驟必須在使用 Play SDK 任何方法前先執行`
```java
PlaySDK.init(getApplication());
```

> 再次提醒只有先初始化 Play SDK 才能設定與使用 Play SDK 的功能

### 需設定 Facebook SDK 所需的 Meta Data
設定 Facebook App ID
```xml
<meta-data
  android:name="com.facebook.sdk.ApplicationId"
  android:value="@string/facebook_app_id" />
```

需加入 Facebook App ID 到 Value/String.xml
```xml
<!-- Facebook App 的 ID 做來驗證登入用 -->
<string name="facebook_app_id">填入 Facebook App ID</string>
```

# 需要設定遊戲外部引用的 Library
### 需要在遊戲外部引用的 Library Project
- Play SDK

### 需要在 Play SDK 外部引用的 Library Project
- appcompat_v7
- Facebook SDK 3.16
- google-play-services_lib

# 需要設定 Play SDK 連接 Server 初始化的資料
在使用 Play SDK 功能前需要先設定橋接到 Server 的相關設定，通常會設置在 App 啟動頁的 onResume 中，確保 Play SDK 初始化連接到 Server 的設定成功
```java
// Play SDK 初始化(如果在 AndroidManifest.xml 中已設定 Application，則不需要執行此步驟)
PlaySDK.init(getApplication());

// 建立 Play SDK 物件
if (playSDK == null) {
  playSDK = PlaySDK.getInstance();
}

// 設定目前是否為測試平台 ( true: 測試平台 / false: 正式平台 )
PlaySDK.isTestEnvironment = true;

// 設定是否開啟 Log 訊息 ( true: 開啟 / false: 關閉 )
playSDK.config().hasDisplayLog(true);

// 設定連接伺服器網址，第一個參數為正式平台，第二個參數為測試平台
playSDK.config().setConnectDomain("http://connect.9388.com/app",
    "http://connect-t.9388.com/app");

// 設定付款伺服器網址，第一個參數為正式平台，第二個參數為測試平台
playSDK.config().setPaymentDomain("http://payment.9388.com/app",
    "http://payment-t.9388.com/app");

// 設定連接伺服器的公鑰
playSDK.config().setConnectPublicKey("y6s84zrq8z");

// 設定付款伺服器的公鑰
playSDK.config().setPayPublicKey("ae4fbr8ngr");

// 設定遊戲名稱 ( Server 端接收到的 Game Tag )
playSDK.config().setGame("hunter");

// 設定遊戲版號
playSDK.config().setGameVersion("1.0.5");

// 設定要進入的遊戲伺服器 (這邊可以在選擇伺服器的時候，再呼叫使用即可)
playSDK.config().setServerID("1");
```

# API 回調
### 回調事件 ResponseObserver
目前所有 API 回調方式都會是藉由 ResponseObserver 實作的 interface 去處理相對應的事件，
因此可由每個 ResponseObserver 傳回來的 ResponseClient 物件得知目前 API 處理狀況
```java
playSDK.api().doFastLogin(new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 所有失敗狀況都會在這邊處理(包含網路失敗)
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 只有成功才會在這邊處理
  }
});
```

### 回調物件 ResponseClient
#### Public
| Method | Return | Description	|
| --- | --- | --- |
| isSuccess | Booleam | 取得是否成功 |
| getCode_number | Int | 取得成功或錯誤代號 |
| getEvent_code | Int | 取得事件代號 |
| getEvent_name | String | 取得事件名稱 |
| getError_message | String | 取得錯誤訊息<br>(成功的話沒有) |
| getData | String | 取得成功回應資料<br>(只有在取得productID的時候才有資料) |
| getResponse_timestamp | String | 取得完成的時間點 |
| getProducts_id | ArrayList<String> | 商品列表 <br/>(只有在 getProductID API中成功回調才有該參數)|
| displayInfo(isSuccess, title) | void | 用 Log 輸出所有 ResponseClient 內的資料 <br>isSuccess: true/false <br>title: 填入顯示的前綴字 |

# API 列表
<!-- + [doFastRegister](../SDK_API/doFastRegister.md)
+ [doFastLogin](../SDK_API/doFastLogin.md)
+ [doFastBindFacebook](../SDK_API/doFastBindFacebook.md)
+ [doFastBindEmail](../SDK_API/doFastBindEmail.md)
+ [doFastPlay](../SDK_API/doFastPlay.md)
+ [doFacebookRegister](../SDK_API/doFacebookRegister.md)
+ [doFacebookLogin](../SDK_API/doFacebookLogin.md)
+ [doFacebookPlay](../SDK_API/doFacebookPlay.md)
+ [doEmailRegister](../SDK_API/doEmailRegister.md)
+ [doEmailLogin](../SDK_API/doEmailLogin.md)
+ [doEmailPlay](../SDK_API/doEmailPlay.md)
+ [getProductID](../SDK_API/getProductID.md)
+ [doDeleteLoginData](../SDK_API/doDeleteLoginData.md)
+ [doUserAccept](../SDK_API/doUserAccept.md)
+ [displayUserBindView](../SDK_API/displayUserBindView.md) -->

 + doFastRegister `(ResponseObserver responseObserver)`
 + doFastLogin `(ResponseObserver responseObserver)`
 + doFastPlay `(ResponseObserver responseObserver)`
 + doFastBindFacebook `(Activity activity, ResponseObserver responseObserver)`
 + doFastBindGoogle `(ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver  + responseObserver)`
 + doFastBindPlay9388 `(String result, ResponseObserver responseObserver)`
 + doFacebookRegister `(Activity activity, ResponseObserver responseObserver)`
 + doFacebookLogin `(ResponseObserver responseObserver)`
 + doFacebookPlay `(ResponseObserver responseObserver)`
 + doGoogleRegister `(ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver  + responseObserver)`
 + doGoogleLogin `(ResponseObserver responseObserver)`
 + doGooglePlay `(ResponseObserver responseObserver)`
 + doPlay9388Register `(String result, ResponseObserver responseObserver)`
 + doPlay9388Login `(ResponseObserver responseObserver)`
 + doPlay9388Play `(ResponseObserver responseObserver)`
 + doGameControl `(ResponseObserver responseObserver)`
 + doProductID `(ResponseObserver responseObserver)`
 + doLogout `(Activity activity)`
 + startLoginActivity `(Activity activity)`
 + onLoginActivityResult `(Activity activity, int requestCode, int resultCode, Intent data, ResponseObserver responseObserver)`

<!--
如果有其他 Callback 需用下列方式去引用，利用 forgameSDK.email().isEmailActivityResult(requestCode) 去區別回調事件
```java
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  super.onActivityResult(requestCode, resultCode, data);

  if (forgameSDK.email().isEmailActivityResult(requestCode)) {

    forgameSDK.email().onActivityResult(requestCode, resultCode, data,
    new ResponseObserver() {

      @Override
      public void onError(ResponseClient result) {
        // TODO Auto-generated method stub
        失敗的狀況
      }

      @Override
      public void onSuccess(ResponseClient result) {
        // TODO Auto-generated method stub
        成功的狀況
      }

    });
  } else {
    forgameSDK.facebook().onActivityResult(this, requestCode,
    resultCode, data);
  }
}
```
-->
