# 版本說明
| 項目 | 描述 |
| -- | -- |
| 專案名稱 | Play SDK |
| 專案版本 | 1.7.6 |
| 發佈時間 | 2014-12-23 |
| 開發人員 | Neo Hsu |

# 目前提供功能
- Play SDK 登入頁面
- Play SDK 浮動小球
- Play SDK 浮動頁面
- Google In-app Billing for Play SDK
- Google Cloud Messaging for Play SDK

# 修正列表
1. [Fixed Bugs] 修正防止付款物件處於 Null 的機制
2. [Modify] 增加付款 API 參數，投入package參數表示目前為 cb/ob 的 APK
3. [Added] 增加直屏或橫屏的顯示設定(Login/SDK)

  ```java
  // 設定登入顯示頁面是否橫向顯示 (true:橫向/false:直向)
  playSDK.config().setLoginLandscape(true);

  // 設定 SDK 顯示頁面是否橫向顯示 (true:橫向/false:直向)
  playSDK.config().setSDKLandscape(false);
  ```

# 需要啟動的權限＆服務
關於 AndroidManifest.xml 的設定與調整

### 需啟動的權限
```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
<uses-permission android:name="android.permission.VIBRATE" />
<uses-permission android:name="com.android.vending.BILLING" />

<!-- Google Cloud Messaging 所需權限，需要填入目前正在使用的 package name  -->
<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
<uses-permission android:name="`[填入目前正在使用的 package name]`.permission.C2D_MESSAGE" />
```

### 需設定 Play SDK 用到的 Activity
```xml
<!-- Play SDK 登入頁面 -->
<activity android:name="com.forgame.playsdk.activity.LoginActivity" />
<!-- Play SDK 浮動頁面 -->
<activity android:name="com.forgame.playsdk.activity.PlaySDKActivity" />
<!-- Facebook login 使用的頁面 -->
<activity
  android:name="com.facebook.LoginActivity"
  android:label="@string/app_name"
  android:theme="@android:style/Theme.Translucent.NoTitleBar" />
```

### 需設定 Play SDK 初始化
在 AndroidManifest.xml 的 Application 中增加 android:name 參數，並使用 com.forgame.playsdk.PlayApplication
```xml
<application
  android:name="com.forgame.playsdk.PlayApplication"
  android:allowBackup="true"
  android:icon="@drawable/ic_launcher"
  android:label="@string/app_name"
  android:theme="@style/AppTheme" >
  ...
  ...
  ...
</application>
```

### 需設定所需的 Meta Data
```xml
<!-- Google Play Service 所需要判斷版本的設定 -->
<meta-data
  android:name="com.google.android.gms.version"
  android:value="@integer/google_play_services_version" />

<!-- Facebook SDK 所需要的設定，用來建立對應的 Facebook App ID -->
<meta-data
  android:name="com.facebook.sdk.ApplicationId"
  android:value="@string/facebook_app_id" />
```
**需加入 Facebook App ID 到 Value/String.xml**
```xml
<!-- 填入 Facebook SDK 所需的 Facebook App ID 設定 -->
<string name="facebook_app_id">`填入 Facebook App ID`</string>
```

### 需啟動的服務
```xml
<service
  android:name="com.forgame.playsdk.util.service.PlaySDKCircleView"
  android:exported="true" />
<service
  android:name="com.forgame.playsdk.util.service.AngelCircleView"
  android:exported="true" />
<service android:name="org.OpenUDID.OpenUDID_service" >
  <intent-filter>
    <action android:name="org.OpenUDID.GETUDID" />
  </intent-filter>
</service>

<!-- Google Cloud Messaging 註冊接收器，需要填入目前正在使用的 package name  -->
<receiver
  android:name="com.forgame.playsdk.util.management.MyGCMBroadcastReceiver"
  android:permission="com.google.android.c2dm.permission.SEND" >
  <intent-filter>
    <!-- Receives the actual messages. -->
    <action android:name="com.google.android.c2dm.intent.RECEIVE" />
    <action android:name="com.google.android.c2dm.intent.REGISTRATION" />

    <category android:name="`[填入目前正在使用的 package name]`" />
  </intent-filter>
</receiver>
<!-- 啟動 Google Cloud Messaging 服務  -->
<service android:name="com.forgame.playsdk.util.service.GcmIntentService" />
```

# 需要設定外部引用的 Library
### 需要在 PlaySDK 外部引用的 Library Project
- Facebook SDK
- Google Play Services Library
- android-support-v7-appcompat

### 需要在遊戲外部引用的 Library Project
- PlaySDK

# 需要設定 Play SDK 初始化的資料
在使用 Play SDK 功能前需要先設定橋接到 Server 的相關設定，通常會設置在 App 啟動頁的 onResume 中，確保 Play SDK 初始化連接到 Server 的設定成功
```java
// 宣告 Play SDK
PlaySDK playSDK;

// 建立 Play SDK 物件
if (playSDK == null) {
  playSDK = PlaySDK.getInstance();
}

// 設定目前是否為測試平台 ( true: 測試平台 / false: 正式平台 )
playSDK.config().isTestEnvironment(true);

// 設定是否開啟 Log 訊息 ( true: 開啟 / false: 關閉 )
playSDK.config().hasDisplayLog(true);

// 設定連接伺服器網址，第一個參數為正式平台，第二個參數為測試平台
playSDK.config().setConnectDomain("http://connect.9388.com/app",
    "http://connect-t.9388.com/app");

// 設定付款伺服器網址，第一個參數為正式平台，第二個參數為測試平台
playSDK.config().setPaymentDomain("http://payment.9388.com/app",
    "http://payment-t.9388.com/app");

// 設定連接伺服器的公鑰
playSDK.config().setConnectPublicKey("連接伺服器公鑰");

// 設定付款伺服器的公鑰
playSDK.config().setPayPublicKey("付款伺服器公鑰");

// 設定遊戲名稱 ( Server 端接收到的 Game Tag )
playSDK.config().setGame("遊戲名稱");

// 設定遊戲版號
playSDK.config().setGameVersion("遊戲版號");

// 設定遊戲伺服器(未選擇伺服器設定 0，選擇伺服器設定伺服器編號)
playSDK.config().setServerID("0");

// 是否啟動 SDK 小球自動淡化效果 (true:顯示後兩秒自動淡化/false:點擊才淡化)
playSDK.config().setSDKFadeOut(false);

// 設定登入顯示頁面是否橫向顯示 (true:橫向/false:直向)
playSDK.config().setLoginLandscape(true);

// 設定 SDK 顯示頁面是否橫向顯示 (true:橫向/false:直向)
playSDK.config().setSDKLandscape(false);

// 設定提示視窗顯示的 icon
playSDK.config().setDisplayGameIcon(R.drawable.ic_launcher);

// 設定提示視窗顯示 Title
playSDK.config().setDisplayGameName("遊戲名稱");

// 設定推播訊息點擊後會跳到的頁面(通常設定 App 啟動頁)
playSDK.config().setGCMIntent("遊戲的啟動頁");

// 設定是否永久開啟第三方儲值 (true:永久啟動/false:根據 SDK 資料啟動)
playSDK.config().setThirdPartyPay(false);

// 設定 Google 付款金鑰
playSDK.config().setPurchasePublickey("Google 付款金鑰");

// 設定 SDK 小球 X 座標
playSDK.config().setCircleX("50");

// 設定 SDK 小球 Y 座標
playSDK.config().setCircleY("50");
```

# 登入頁面
Play SDK 提供登入頁面的 Activity 給遊戲使用，並藉由回調的 onActivityResult 去處理相對應的動作
### 啟動登入頁面
```java
// Play SDK 啟動登入頁面
playSDK.api().startLoginActivity(this);
```
### 處理登入回調事件
從原本啟動登入頁的 Activity 中，加入 onActivityResult 事件並執行以下處理
```java
// Play SDK 登入頁面的回調機制
playSDK.api().onLoginActivityResult(this, requestCode, resultCode, data,
new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 連線錯誤處理方式
    // [範例為再次重新啟動登入頁面]
    playSDK.api().startLoginActivity(LoginActivity.this);
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    if (responseClient.getEvent_code() == EventCode.EVENT_LOGIN_ERROR) {
      // 這邊處理異常離開登入頁面的狀態，譬如直接按Back鍵或非正常程序 Finish 掉 Login 畫面
      // [範例為再次重新啟動登入頁面]
      playSDK.api().startLoginActivity(LoginActivity.this);
    } else {
      // 這邊處理正常離開登入頁面的狀態
      // [通常會處理跳到選擇遊戲伺服器的頁面]
      // [範例表示的狀況為使用者直接選擇了伺服器並點選進入遊戲的狀況]

      // [範例：玩家選擇遊戲伺服器]
      // 選擇遊戲伺服器
      playSDK.config().setServerID("1");

      // [範例：玩家點選進入遊戲]
      // 執行進入遊戲事件
      playSDK.api().doPlayerPlay(new ResponseObserver() {

        @Override
        public void onError(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          // 執行失敗的狀況
        }

        @Override
        public void onSuccess(ResponseClient responseClient, int event) {
          // TODO Auto-generated method stub
          if (responseClient.isSuccess()) {
            // 執行成功的狀況
            // [範例：跳置遊戲內]
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
          } else {
            // 執行失敗的狀況
          }
        }
      });
    }
  }
});
```

# 設定遊戲伺服器跳轉(Server ID)
目前遊戲流程如下：
> 登入頁面 => 選擇伺服器頁面 => 遊戲頁面

1. 登入頁面：playSDK.api().startLoginActivity(this); 即可進入登入頁面
2. 登入成功後到選擇伺服器頁面：
  - 當選擇完伺服器後，調整 Server ID => playSDK.config().setServerID("伺服器編號");
  - 點選進入遊戲前再呼叫 => playSDK.api().doPlayerPlay(new ResponseObserver(){....});
3. 執行 playSDK.api().doPlayerPlay 成功才正式進入遊戲

# Play SDK 浮動小球
由於浮動小球是啟動於 Service 中，因此需在要出現小球的 Activity 上設定該小球服務
> 浮動小球必須登入成功後再呼叫 playSDK.api().doGameControl(new ResponseObserver() {...}); 去取得浮動小球的資料，才可以顯示浮動小球

啟動 Play SDK 小球服務
onCreate 的事件中需加入：
```java
// 啟動 SDK 小球服務
playSDK.api().startSDKViewService(this);

// 取得 Play SDK 小球控制設定資訊
playSDK.api().doGameControl(new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 執行失敗的狀況
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 執行成功的狀況
    playSDK.api().openSDKView(MainActivity.this);
  }
});
```
開啟 Play SDK 小球
onResume 的事件中需加入：
```java
playSDK.api().openSDKView(this);
```
關閉 Play SDK 小球
onPause 的事件中需加入：
```java
playSDK.api().closeSDKView(this);
```
關閉 Play SDK 小球服務
onDestroy 的事件中需加入：
```java
playSDK.api().stopSDKViewService(this);
```

# 建立 Play SDK 的 Google Cloud Messaging
登入後才處理相關設定，否則 RegisterID 無法對應遊戲使用者
```java
// 註冊推播服務
playSDK.gcm().doGCMRegister();
```

# 直接啟動第三方儲值頁面
有時候會針對下載 APK 包時啟用遊戲內點擊按鈕永久開啟第三方儲值頁面的需求，需處理以下設定
```java
// 啟動永久開啟第三方儲值設定
playSDK.config().setThirdPartyPay(true);

// 開啟第三方儲值的頁面 Activity
Intent intent = new Intent(this, com.forgame.playsdk.activity.PlaySDKActivity.class);
Bundle bundle = new Bundle();
bundle.putString("url", playSDK.host().getPayUrl());
intent.putExtras(bundle);
intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(intent);
```

# 付款機制
**付款處理流程**
<font color="#d9534f">單一 Activity (Singleton) 要在 AndroidManifest 中加入 android:launchMode="singleTask"，為了讓 onActivityResult 順利執行！</font>
![付款處理流程](../Images/ForgameSDK_In-App_flow.png)
在付款頁面載入的時候先初始化付款機制
```java
// 初始化付款機制
playSDK.purchaseControl().init(this);
```
點擊商品後執行下面方法進入 Google 付款機制
```java
playSDK.purchaseControl().purchaseFlow(this, "商品 ID", new PurchaseResponseObserver() {
  @Override
  public void onError(String msg, int event) {
    // TODO Auto-generated method stub
    // 付款失敗處理
  }

  @Override
  public void onSuccess(String msg, int event) {
    // TODO Auto-generated method stub
    // 付款成功處理
  }

});
```
在付款頁面的 Activity 需覆寫 onActivityResult 事件回傳 Google 購買成功
```java
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  // 設定付款機制的回調機制
  if (!playSDK.purchaseControl().getHasActivityResult(requestCode, resultCode, data)) {
    super.onActivityResult(requestCode, resultCode, data);
  } else {

  }
}
```
在預付款頁面關閉的時候結束付款機制（ onDestroy ）
```java
// 停止關閉付款機制
playSDK.purchaseControl().dispose();
```
補單機制的 API 呼叫窗口
```java
playSDK.purchaseControl().checkPurchase(this);
```

# API 回調
### 回調事件 ResponseObserver
目前所有 API 回調方式都會是藉由 ResponseObserver 實作的 interface 去處理相對應的事件，
因此可由每個 ResponseObserver 傳回來的 ResponseClient 物件得知目前 API 處理狀況
```java
playSDK.api().doPlayerPlay(new ResponseObserver() {

  @Override
  public void onError(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 所有失敗狀況都會在這邊處理(包含網路失敗)
  }

  @Override
  public void onSuccess(ResponseClient responseClient, int event) {
    // TODO Auto-generated method stub
    // 只有成功才會在這邊處理
  }
});
```

### 回調物件 ResponseClient
#### Public
| Method | Return | Description	|
| --- | --- | --- |
| isSuccess | Booleam | 取得是否成功 |
| getCode_number | Int | 取得成功或錯誤代號 |
| getEvent_code | Int | 取得事件代號 |
| getEvent_name | String | 取得事件名稱 |
| getError_message | String | 取得錯誤訊息<br>(成功的話沒有) |
| getData | String | 取得成功回應資料 |
| getResponse_timestamp | String | 取得完成的時間點 |
| getProducts_id | ArrayList<String> | 商品列表 <br/>(只有在 getProductID API中成功回調才有該參數)|
| displayInfo(isSuccess, title) | void | 用 Log 輸出所有 ResponseClient 內的資料 <br>isSuccess: true/false <br>title: 填入顯示的前綴字 |

# API 列表
<!-- + [doFastRegister](../SDK_API/doFastRegister.md)
+ [doFastLogin](../SDK_API/doFastLogin.md)
+ [doFastBindFacebook](../SDK_API/doFastBindFacebook.md)
+ [doFastBindEmail](../SDK_API/doFastBindEmail.md)
+ [doFastPlay](../SDK_API/doFastPlay.md)
+ [doFacebookRegister](../SDK_API/doFacebookRegister.md)
+ [doFacebookLogin](../SDK_API/doFacebookLogin.md)
+ [doFacebookPlay](../SDK_API/doFacebookPlay.md)
+ [doEmailRegister](../SDK_API/doEmailRegister.md)
+ [doEmailLogin](../SDK_API/doEmailLogin.md)
+ [doEmailPlay](../SDK_API/doEmailPlay.md)
+ [getProductID](../SDK_API/getProductID.md)
+ [doDeleteLoginData](../SDK_API/doDeleteLoginData.md)
+ [doUserAccept](../SDK_API/doUserAccept.md)
+ [displayUserBindView](../SDK_API/displayUserBindView.md) -->

 + doFastRegister `(ResponseObserver responseObserver)`
 + doFastLogin `(ResponseObserver responseObserver)`
 + doFastPlay `(ResponseObserver responseObserver)`
 + doFastBindFacebook `(Activity activity, ResponseObserver responseObserver)`
 + doFastBindGoogle `(ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver  + responseObserver)`
 + doFastBindPlay9388 `(String result, ResponseObserver responseObserver)`
 + doFacebookRegister `(Activity activity, ResponseObserver responseObserver)`
 + doFacebookLogin `(ResponseObserver responseObserver)`
 + doFacebookPlay `(ResponseObserver responseObserver)`
 + doGoogleRegister `(ConnectionResult mConnectionResult, GoogleApiClient mGoogleApiClient, Activity activity, ResponseObserver  + responseObserver)`
 + doGoogleLogin `(ResponseObserver responseObserver)`
 + doGooglePlay `(ResponseObserver responseObserver)`
 + doPlay9388Register `(String result, ResponseObserver responseObserver)`
 + doPlay9388Login `(ResponseObserver responseObserver)`
 + doPlay9388Play `(ResponseObserver responseObserver)`
 + doGameControl `(ResponseObserver responseObserver)`
 + doProductID `(ResponseObserver responseObserver)`
 + doLogout `(Activity activity)`
 + startLoginActivity `(Activity activity)`
 + onLoginActivityResult `(Activity activity, int requestCode, int resultCode, Intent data, ResponseObserver responseObserver)`
