# Purchase Control
Google In App Purchase(IAP)

## Initial Google IAP (onCreate)
```java
playSDK.purchaseControl().init(Activity);
```

## Continue to purchase product
```java
playSDK.purchaseControl().purchaseFlow(Activity, "product id", new PurchaseResponseObserver() {

  @Override
  public void onError(String msg, int event, String productPrice, Purchase purchase) {
    // purchase contains purchase information
  }

  @Override
  public void onSuccess(String msg, int event, String productPrice, Purchase purchase) {
    // purchase contains purchase information
  }

});
```

#### Use the following get methods to retrieve purchase information:

![Purchase Get Methods](../Images/PurchaseGetMethods.PNG)

The useful ones would be `getItemType` and `getSku`.

#### The method `toString` will also retrieve a group of useful information:

```
PurchaseInfo(type:inapp):{
	"packageName":"com.forgame.mmxd",
	"orderId":"transactionId.android.test.purchased",
	"productId":"android.test.purchased",
	"developerPayload":"bGoa+V7g\/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ",
	"purchaseTime":0,
	"purchaseState":0,
	"purchaseToken":"inapp:com.forgame.mmxd:android.test.purchased"
}
```

## get the feedback of purchase and handle next step (onActivityResult)
```java
if (!playSDK.purchaseControl().getHasActivityResult(requestCode, resultCode, data)) {
  super.onActivityResult(requestCode, resultCode, data);
} else {

}
```

## Destroy Google IAP (onDestroy)
```java
playSDK.purchaseControl().dispose();
```
