# Play SDK Library Project

This document records the guideline of SDK and is the reference for maintainance and consultation with factory.

### Author
+ [Neo Hsu](mailto:Neo.Hsu@forgame.com)

### Maintain
+ [Carol Liu](mailto:carol@9388.com)

### Edited
+ [Ruslan Aliyev](mailto:ruslan.aliyev@global.forgame.com)

---
Copyright © 2016 Forgame.com. All right reserved.
