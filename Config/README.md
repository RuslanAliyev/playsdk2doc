# Config
#### Public
| Method | Requests | Return | Description |
| --- | --- | --- | --- |
| hasDisplayLog | Boolean | void | log display setting<br> ( true: open / false: hidden ) |
| isTestEnvironment | Boolean | void | environment setting<br> ( true: test / false: normal ) |
| isAdminMode | Boolean | void | set mode of GM Admin |
| setConnectDomain | String,  String | void | set login api url<br>the first parameter is normal server<br>the second parameter is test server |
| setPaymentDomain | String,  String | void | set pay api url<br>the first parameter is normal server<br>the second parameter is test server |
| setGame | String | void | set game name |
| setGameVersion | String | void | set game version |
| setServerID | String | void | set game server id |
| setMobileSystem | String | void | set mobile system |
| setConnectPublicKey | String | void | set publickey of login api |
| setPayPublicKey | String | void | set publickey of pay api |
| setPurchasePublickey | String | void | set publickey of Google's purchace |
| setCircleX | Int | void | set initial X-coordinate of 9388 ball |
| setCircleY | Int | void | set initial Y-coordinate of 9388 ball |
| setPlaySDKCircleText | String | void | set display text of 9388 ball |
| setSDKFadeOut | Boolean | void | set fade out effects after display 9388 ball<br>(true:fade out automatically when show out after 2 seconds/false:fade out when click 9388 ball) |
| setLoginLandscape | Boolean | void | set landscape of login page<br>(true:horizontal/false:vertical) |
| setSDKLandscape | Boolean | void | set landscape of 9388 ball<br>(true:horizontal/false:vertical) |
| setCurrentLoginType | Int | void | set login type<br>(55:Fast/5:Google/2:Facebook/1:Play9388) |
| setThirdPartyPay | Boolean | void | set permission status on third party payment<br>(true:always display/false:display depends on server setting) |
| setGCMRegID | String | void | set GCM Register ID |
| setGCMSenderID | String | void | set GCM Sender ID |
| setDisplayGameName | String | void | set title of notice windows header |
| setDisplayGameIcon | int | void | set icon of notice windows |
| setLauncherIntentString | String | void | set launch activity when GCM push and click the message at screen |
| isCbPackage | Boolean | void | set CB package: True |
| doClearAll | | void | clear all data |
| setAPKMode | Int | void | set mode of APK |
